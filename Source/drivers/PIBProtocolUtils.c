//******************************************************************************
//
// PIBProtocolUtils.c: PIB Communication protocol routines
//
//  Copyright (c) 2016, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2016-Oct-04     DD        Initial Version
//
//******************************************************************************

#include "PIBProtocolUtils.h"

//#pragma package(smart_init)


//
// Private Declarations
//

#define DEFAULT_SOL_CTRL		0x00
#define DEFAULT_CONT_CTRL		0x00
#define DEFAULT_SENS_PWR_CTRL	0x01
#define DEFAULT_SOL_TIMEOUT		10		// seconds


#define PIB_NBR_RSP				3   // Total number of PIB RESP_... defines
#define RS485_PKT_NBR			64

#define MPL_COMM_TIMEOUT		50	//ms
static TIMERHANDLE thPibTimer;
//static TIMERHANDLE thSolenoidSecTimer;

typedef struct {
    BYTE pktPibHdr;     // Expected header byte
    BYTE cmdRespByte;   // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
    BYTE expDataLen;    // Required value for header pktLen param
} PIB_PKT_INFO;

static PIB_PKT_INFO m_pktInfoMaster[ PIB_NBR_RSP] = {
	{ PIB_M_CMD_HDR, PIB_GET_DATA, 0 },
    { PIB_M_CMD_HDR, PIB_SET_PARAMS, sizeof(PIB_SET_PARAM_CMD_PKT) },
	{ PIB_M_CMD_HDR, COM_GET_VERSIONS, 0 },
};

static PIB_PKT_INFO m_pktInfoSlave[ PIB_NBR_RSP] = {
	{ PIB_S_CMD_HDR, PIB_GET_DATA, 0 },
    { PIB_S_CMD_HDR, PIB_SET_PARAMS, sizeof(PIB_SET_PARAM_CMD_PKT) },
	{ PIB_S_CMD_HDR, COM_GET_VERSIONS, 0 },
};

static PIB_PKT_INFO	*m_pktInfo;
static BYTE pibRespHeader;

typedef struct{
	BOOL solenoidActive;
	TIMERHANDLE thSolenoidSecTimer;
}SOLENOID_STATE_PARAM;


static BYTE rs485Packet[RS485_PKT_NBR];
static BOOL  havePIBPkt = FALSE;
//static WORD tmpWord;

static PIB_PKT_HDR* pktPibHdr;
static BYTE* pktPibData;
static PIB_RSP_PKT* pPibParamRsp;
static PIB_VERSIONS_RSP_PKT* pPibVerRsp;

static BYTE* pPibCRC;

static BYTE currentSolCtrl;		// activate/deactivate PIB solenoids
static BYTE currentContCtrl;	// activate/deactivate PIB contacts
static WORD currentSolTimeout;	// maximum time a solenoid can be in active state, measured in seconds.

const WTTTS_RATE_TYPE getSolTimeout = SRT_SOLENOID_TIMEOUT;
const BYTE PTS_ADDRESS[NBR_PTS_BOARDS]={1,2,3,4};	//	PTS: address range of 1 through 16.

static SOLENOID_STATE_PARAM sSolState;

//static BYTE pibCommBuff[4*sizeof(PTS_GET_PARAM_RSP_PKT)];
typedef enum{
	MPL_COMM_START,
	MPL_GET_PIB_PARAMS,
	MPL_READ_PIB_PARAMS,
	MPL_GET_PTS_PARAMS,
	MPL_READ_PTS_PARAMS,
	MPL_COMM_IDLE,
	NBR_MPL_COMM_STATES
}MPL_COMM_STATE;

MPL_COMM_STATE	mplCommState;

MPL_OP_MODES mplOpModes;


BYTE pibRole;
BYTE lastRcvdCmd;
//******************************************************************************
//
//  Function: ReadDeviceRole
//
//  Arguments: none
//
//  Returns:
//
//  Description:  reads pin SW1 determine the device role: MPLS_MASTER(pin is '1') or MPLS_SLAVE(pin is '0').
//				This function must be called only at power up.
//******************************************************************************
void ReadDeviceRole(void)
{
#ifdef SET_MASTER_PIB
	SetPibRole(MASTER_PIB);
#elif SET_SLAVE_PIB
	SetPibRole(SLAVE_PIB);
#else

	if(ReadInputPin(MS_SETUP_IN))
	{
		SetPibRole(MASTER_PIB);
	}
	else
	{
		SetPibRole(SLAVE_PIB);
	}
#endif
}

void SetPibRole(BYTE pibMasterSlave)
{

	switch (pibMasterSlave )
	{
	case MASTER_PIB:
		pibRole = MASTER_PIB;
		m_pktInfo = m_pktInfoMaster;
		pibRespHeader = PIB_M_RESP_HDR;
		break;
	case SLAVE_PIB:
		pibRole = SLAVE_PIB;
		m_pktInfo = m_pktInfoSlave;
		pibRespHeader = PIB_S_RESP_HDR;
	default:
		// todo
		break;
	}
}



void InitPIBInterface(void)
{

	InitRs485Interface();
	currentSolCtrl = DEFAULT_SOL_CTRL;
	currentContCtrl = DEFAULT_CONT_CTRL;
	currentSolTimeout = DEFAULT_SOL_TIMEOUT;

	thPibTimer = RegisterTimer();
	StartTimer( thPibTimer );

	sSolState.solenoidActive = FALSE;
	sSolState.thSolenoidSecTimer = RegisterSecTimer();
	StartSecTimer(sSolState.thSolenoidSecTimer);
	mplOpModes = MODE_HUNTING;
	//mlpCommStateInit();
	lastRcvdCmd = 0;
}




//******************************************************************************
//
//  Function: HavePIBPacket
//
//  Arguments: 	BYTE *pBuff - pointer to incomming data buffer
//				WORD buffLen - length of the data buffer to check.
//  Returns: BOOL - TRUE if a valid packet is found, FALSE otherwise
//
//  Description: examine the incoming data for valid data packet.
//
//******************************************************************************
BOOL HavePIBPacket(BYTE *pBuff, WORD buffLen)
{
	// Scans the passed buffer for a PIB command or response. Returns true if
	// a packet is found, in which case pktPibHdr and pktPibData pointers are asigned.
	// Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePIBPkt = FALSE;

    if( buffLen < (WORD)MIN_PIB_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_PIB_PKT_LEN <= buffLen )
       {
    		ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if( pBuff[bytesSkipped] != m_pktInfo[0].pktPibHdr)
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           pktPibHdr = (PIB_PKT_HDR*)&(pBuff[bytesSkipped]);

           for(iInfoItem = 0; iInfoItem < PIB_NBR_RSP; iInfoItem++ )
           {
               if( m_pktInfo[iInfoItem].cmdRespByte != pktPibHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_PIB_PKT_LEN + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
              // memcpy( &pktPibHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
        	   pktPibHdr = (PIB_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
                  // memcpy( &pktPibData, &( pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ), dataLen );
            	   pktPibData = &pBuff[bytesSkipped + sizeof( PIB_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePIBPkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePIBPkt;
}



//******************************************************************************
//
//  Function: ExecuteTecCommand
//
//  Arguments: none
//
//  Returns: none
//
//  Description: execute the received command.
//
//******************************************************************************
void ExecuteTecCommand(void)
{


	ResetWatchdog();
	switch(pktPibHdr->pktType)
	{

	case PIB_GET_DATA:
		ExecGetParams();
		lastRcvdCmd = PIB_GET_DATA;
		break;
	case PIB_SET_PARAMS:
		ExecSetParams();	// set parameters
		ExecGetParams();	// send the response
		lastRcvdCmd = PIB_SET_PARAMS;
		break;
	case COM_GET_VERSIONS:
		ExecGetVersion();
		lastRcvdCmd = COM_GET_VERSIONS;
		break;
	default:
		break;
	}
}

//******************************************************************************
//
//  Function: ExecSetParams
//
//  Arguments: none
//
//  Returns: none
//
//  Description: set PIB parameters.
//
//******************************************************************************
void ExecSetParams(void)
{
	PIB_SET_PARAM_CMD_PKT* pSetParamsCmd;

	pSetParamsCmd = (PIB_SET_PARAM_CMD_PKT*) pktPibData;

	if(pSetParamsCmd->solenoidCtrl <= TOTAL_SOLENOIDS)
	{
		if(pSetParamsCmd->solenoidCtrl == 0)
		{
			ResetSolenoidState();
			currentSolCtrl = 0;
		}
		else
		{
			currentSolTimeout = pSetParamsCmd->solenoidTimeout;
			currentSolCtrl = pSetParamsCmd->solenoidCtrl;
			SetSolenoidStateResetOthers(currentSolCtrl-1);
			ResetSecTimer(sSolState.thSolenoidSecTimer, currentSolTimeout);
			sSolState.solenoidActive = TRUE;
		}
	}

	currentContCtrl = pSetParamsCmd->contactCtrl;
	SetContactByBitMask(currentContCtrl);

}



//******************************************************************************
//
//  Function: ExecGetParams
//
//  Arguments: none
//
//  Returns: none
//
//  Description: reports PIB parameters.
//
//******************************************************************************
void ExecGetParams(void)
{
	WORD wResult;
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibParamRsp = (PIB_RSP_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_RSP_PKT)];

	pktPibHdr->pktHdr = pibRespHeader;
	pktPibHdr->pktType = PIB_GET_DATA;
	pktPibHdr->dataLen = sizeof( PIB_RSP_PKT );

	pPibParamRsp->solenoidStatus = currentSolCtrl;
	pPibParamRsp->contactStatus = currentContCtrl;
	wResult = getBatterVoltage_mV();
	pPibParamRsp->batVoltage[LOW_BYTE]= LOBYTE(wResult);
	pPibParamRsp->batVoltage[HIGH_BYTE]= HIBYTE(wResult);
	pPibParamRsp->solenoidCurrent = getSolenoidCurrent();
	pPibParamRsp->solenoidTimeout = currentSolTimeout;

	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_RSP_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_RSP_PKT)+1);

}
//******************************************************************************
//
//  Function: ExecGetParams
//
//  Arguments: none
//
//  Returns: none
//
//  Description: reports PIB parameters.
//
//******************************************************************************
void ExecGetVersion(void)
{
	// Initialise the pointers
	pktPibHdr = (PIB_PKT_HDR*)rs485Packet;
	pPibVerRsp = (PIB_VERSIONS_RSP_PKT*)&rs485Packet[sizeof(PIB_PKT_HDR)];
	pPibCRC	= (BYTE*)&rs485Packet[sizeof(PIB_PKT_HDR)+sizeof(PIB_VERSIONS_RSP_PKT)];

	pktPibHdr->pktHdr = pibRespHeader;
	pktPibHdr->pktType = COM_GET_VERSIONS;
	pktPibHdr->dataLen = sizeof( PIB_VERSIONS_RSP_PKT );

	if(pibRole == MASTER_PIB)
	{
		pPibVerRsp->devSerNum[0]= 1;
		pPibVerRsp->devSerNum[1]= 1;
		pPibVerRsp->devSerNum[2]= 1;
		pPibVerRsp->devSerNum[3]= 1;

		pPibVerRsp->hardVer[0] = 1;
		pPibVerRsp->hardVer[1] = 0;
		pPibVerRsp->hardVer[2] = 0;
		pPibVerRsp->hardVer[3] = 0;

	}
	else
	{
		pPibVerRsp->devSerNum[0]= 2;
		pPibVerRsp->devSerNum[1]= 2;
		pPibVerRsp->devSerNum[2]= 2;
		pPibVerRsp->devSerNum[3]= 2;

		pPibVerRsp->hardVer[0] = 2;
		pPibVerRsp->hardVer[1] = 0;
		pPibVerRsp->hardVer[2] = 0;
		pPibVerRsp->hardVer[3] = 0;


	}

	pPibVerRsp->frmVers[0] = MAJOR_VERSION;
	pPibVerRsp->frmVers[1] = MINOR_VERSION;
	pPibVerRsp->frmVers[2] = BUILD_NUMBER;
	pPibVerRsp->frmVers[3] = REL_DBG;



	*pPibCRC = CalculateCRC(rs485Packet, sizeof(PIB_PKT_HDR)+sizeof(PIB_VERSIONS_RSP_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(PIB_PKT_HDR)+sizeof(PIB_VERSIONS_RSP_PKT)+1);

}

//******************************************************************************
//
//  Function: UpdateSolenoidState
//
//  Arguments: none
//
//  Returns: none
//
//  Description: It takes care of the solenoid timeout
//
//******************************************************************************
void UpdateSolenoidState(void)
{
	if(	sSolState.solenoidActive == TRUE)
	{
		if(SecTimerExpired(sSolState.thSolenoidSecTimer))
		{
			sSolState.solenoidActive = FALSE;
			currentSolCtrl = 0;
			ResetSolenoidState();

		}
	}
}

//******************************************************************************
//
//  Function: GetLastCmd
//
//  Arguments: none
//
//  Returns: none
//
//  Description: return the last executed command and zeros the variable
//
//******************************************************************************
BYTE GetLastCmd(void)
{
	BYTE bResponse;
	bResponse = lastRcvdCmd;
	lastRcvdCmd = 0;
	return bResponse;
}
