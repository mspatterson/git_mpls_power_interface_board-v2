#ifndef BUILDS_H_
#define BUILDS_H_

#define RELEASE 0
#define DEBUG	1
#define MAJOR_VERSION 	0
#define MINOR_VERSION 	0
#define BUILD_NUMBER  	5
#define REL_DBG			DEBUG

//	#define WATCHDOG_ENABLE	1


//#define DBG_MESSAGES	1
#define SWITCH_ON_OFF		1




#define ADC_RAW_DATA	1
#define ADC_DEBUG_SLOW	1
#define ADC_USED_FIRST	0
#define ADC_USED_LAST	6


//#define SET_MASTER_PIB	1
//#define SET_SLAVE_PIB	1


//#define DBG_MESSAGES_ADC	1
#endif /*BUILDS_H_*/
