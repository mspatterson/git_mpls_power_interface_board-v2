#ifndef ADC10_H_
#define ADC10_H_

#include "..\drivers.h"

void ADC10_Init(void);
void adcReadCh0(void);
void adcReadCh1(void);
WORD getBatterVoltage_mV(void);
BYTE getSolenoidCurrent(void);


#endif /*ADC10_H_*/
