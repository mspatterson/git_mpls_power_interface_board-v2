/*
 * system.c
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#include "system.h"

static BYTE lastReset =1;
//******************************************************************************
// Function name:    Set_System_Clock
//******************************************************************************
// Description:      initialize the MCU clock
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void Set_System_Clock (void)
{


}


//******************************************************************************
// Function name:    SetSystemDeepSleep
//******************************************************************************
// Description:      set the system in deep sleep and cofigure wake up on interrupt from the REED_SWITCH
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************

void SetSystemDeepSleep(void)
{

}


//******************************************************************************
// Function name:    Set_System_Clock_LPM
//******************************************************************************
// Description:      initialize the MCU clock for low power mode
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void Set_System_Clock_LPM (void)
{

}



//******************************************************************************
// Function name:    SetWatchdog
//******************************************************************************
// Description:      set the watchdog to 1sec interval
//					assuming the ACLK is sourced from REF0 	32768 Hz
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void SetWatchdog(void)
{
	WDTCTL = WDT_ARST_1000;
}

//******************************************************************************
// Function name:    ResetWatchdog
//******************************************************************************
// Description:      clear the watchdog counter
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void ResetWatchdog(void)
{
//	WDTCTL = WDT_ARST_1000;
}

//******************************************************************************
// Function name:    StopWatchdog
//******************************************************************************
// Description:      hold the watchdog
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StopWatchdog(void)
{
	WDTCTL = WDTPW + WDTHOLD;
}

/*	to do
 * To get the cause of the last reset(s), use the reset vector generator SYSRSTIV.
 * It will give you on each reading the highest priority reset cause that hasn't already read.
 * see http://e2e.ti.com/support/microcontrollers/msp430/f/166/t/99283.aspx
 */
BYTE GetLastReset(void)
{
	return lastReset;
}

