#ifndef GPIO_H_
#define GPIO_H_



#include "..\drivers.h"

// Date July 2016 - initial release for PCB-00962-01-1v0

//******************************************************************************
// Port 1	-
//******************************************************************************
// P1.0 - RS485_PWR_EN	[Output]
// P1.1 - BSL_TX		[Input] - input during normal operation. This is data transmit in BSL mode.
// P1.2 - EN_12V		[Output]
// P1.3 - EXT_DEV_ADD	[Input]		// plug switch 2
// P1.4 - TCK		   	[Input]	- Debug Port
// P1.5 - TMS		 	[Input] - Debug Port
// P1.6 - TDI		 	[Input] - Debug Port
// P1.7 - TDO			[Output]- Debug Port
#define RS485_PWR_EN	0x00
#define BSL_TX			0x01
#define EN_12V			0x02
#define PLUG_SW2		0x03
#define DBG_TCK			0x04
#define DBG_TMS			0x05
#define DBG_TDI			0x06
#define DBG_TDO			0x07

#define PORT_1_DIR_INIT     0x05
#define PORT_1_VALUE_INT    0x00
#define PORT_1_PERIPH_SEL   0x00

// in POWER DOWN MODE
#define PORT_1_DIR_POWER_DOWN		0x05
#define PORT_1_VALUE_POWER_DOWN    	0x00


//******************************************************************************
// Port 2	-
//******************************************************************************
// P2.0 - BAT_VSENSE  	[Input]	 -  ADC input
// P2.1 - MON_12V		[Input]	 -  ADC input
// P2.2 - BSL_RX		[Input] - input during normal operation. This is data receive in BSL mode.
// P2.3 - CC_OUT_1		[Output]
// P2.4 - CC_OUT_2		[Output]
// P2.5 - CC_OUT_4		[Output]
// P2.6 - CC_OUT_3		[Output]
// P2.7 - MS_SETUP_IN	[Input]		// Master/Slave input
#define BAT_VSENSE		0x10
#define MON_12V			0x11
#define BSL_RX			0x12
#define CC_OUT_1		0x13
#define CC_OUT_2		0x14
#define CC_OUT_4		0x15
#define CC_OUT_3		0x16
//#define PLUG_SW1		0x17
#define MS_SETUP_IN		0x17

#define PORT_2_DIR_INIT     0x78
#define PORT_2_VALUE_INT    0x00
#define PORT_2_PERIPH_SEL  	0x00
// in POWER DOWN MODE set ...
#define PORT_2_DIR_POWER_DOWN     0xF8
#define PORT_2_VALUE_POWER_DOWN    0x00

  
//******************************************************************************
// Port 3	-
//******************************************************************************
// P3.0 - NC1	     	[Output]
// P3.1 - I2C_SDA		[I/O] 		I2C data
// P3.2 - I2C_SCL		[Output]   	I2C clock
// P3.3 - SOL_TEST_EN	[Output]	active high
// P3.4 - UART0TX		[Output]	tx data
// P3.5 - UART0RX 		[Input] 	rx data
// P3.6 - RS485_RX_EN	[Output]	active low
// P3.7 - RS485_TX_EN	[Output]	active high
#define NC1				0x20
#define I2C_SDA			0x21
#define I2C_SCL			0x22
#define SOL_TEST_EN		0x23
#define UART0TX			0x24
#define UART0RX			0x25
#define RS485_RX_EN		0x26
#define RS485_TX_EN		0x27

#define PORT_3_DIR_INIT     0xDF	//(1<<0)|(1<<2)|(1<<3)|(1<<4)|(1<<6)|(1<<7)
#define PORT_3_VALUE_INT    0x00
#define PORT_3_PERIPH_SEL   0x00
// in POWER DOWN MODE set ...
#define PORT_3_DIR_POWER_DOWN     	0xDF	//(1<<0)|(1<<2)|(1<<3)|(1<<4)|(1<<6)|(1<<7)
#define PORT_3_VALUE_POWER_DOWN    	0x00|(1<<6)

   
//******************************************************************************
// Port 4	-
//******************************************************************************
// P4.0 - SOLEN_EN_1 	[Output]	- Solenoid enable output - active high
// P4.1 - SOLEN_EN_2 	[Output]	- Solenoid enable output - active high
// P4.2 - SOLEN_EN_3 	[Output]	- Solenoid enable output - active high
// P4.3 - SOLEN_EN_4 	[Output]	- Solenoid enable output - active high
// P4.4 - SOLEN_EN_5 	[Output]	- Solenoid enable output - active high
// P4.5 - FAULT_12V		[Input]
// P4.6 - SW_LED		[Output]	- active high
// P4.7 - BAT_VSENSE_EN	[Output]	- active high
#define SOLEN_EN_1			0x30
#define SOLEN_EN_2			0x31
#define SOLEN_EN_3			0x32
#define SOLEN_EN_4			0x33
#define SOLEN_EN_5			0x34
#define FAULT_12V			0x35
#define SW_LED				0x36
#define BAT_VSENSE_EN		0x37


//#define PORT_4_DIR_INIT     		(1<<5)|(1<<6)|(1<<7)
#define PORT_4_DIR_INIT     		0xDF
#define PORT_4_VALUE_INT    		0x00
#define PORT_4_PERIPH_SEL			0x00
// in POWER DOWN MODE set all pins as output low
#define PORT_4_DIR_POWER_DOWN		0xDF
#define PORT_4_VALUE_POWER_DOWN		0X00



#define TOTAL_PORT_NUMBERS	4


// get the port number for the pin name
#define GPIO_GET_PORT( a )	( (a) >> 4 )

// get the pin number for the pin name4
#define GPIO_GET_PIN( a )	( (a) & 0x0f )


void SetPinAsPeripheral(BYTE pinName);
void SetPinAsOutput(BYTE pinName);
void SetPinAsInput(BYTE pinName);
void SetOutputPin(BYTE pinName, BOOL bState);
BOOL ReadInputPin(BYTE pinName);
void EnableInputPullUp(BYTE pinName);

void initMPS430Pins( void );
void SetMPS430PinsInPowerDown( void );


#endif /*GPIO_H_*/
