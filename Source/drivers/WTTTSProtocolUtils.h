#ifndef WTTTSProtocolUtilsH
#define WTTTSProtocolUtilsH

#include "drivers.h"

//
// Packet Definitions for WTTTS V2 Communications
// Refer to Specification 153-0084 for further info
//
// Note: the capability exists to perform over-the-air updating of
// the WTTTS firmware. This capability is only implemented in a
// specialized software program. Therefore, all definitions related
// to that ability have been excluded from this file.
//


// Packet formats (inbound and outbound). All structures are byte-aligned
//#pragma pack(push, 1)

// Packet header
typedef struct {
    BYTE pktHdr;       // WTTTS_HDR_... define
    BYTE pktType;      // WTTTS_CMD_... or WTTTS_RESP_... define
    BYTE seqNbr;       // Incremented on each tx packet. Rolls over to zero at 255
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
    WORD timeStamp;    // Packet time, in 10's of msec, since device power-up
} WTTTS_PKT_HDR;



// Stream data packet data
typedef struct {
    BYTE torque000[3];
    BYTE torque180[3];
    BYTE tension000[3];
    BYTE tension090[3];
    BYTE tension180[3];
    BYTE tension270[3];
    BYTE gyro[4];
    BYTE compassX[2];
    BYTE compassY[2];
    BYTE compassZ[2];
    BYTE accelX[2];
    BYTE accelY[2];
    BYTE accelZ[2];
} WTTTS_STREAM_DATA;

// Request for command packet
#define NBR_WTTTS_PRESSURE_BYTES  3

typedef struct {
    BYTE  temperature; // signed value, 0.5C per count with, -64C to +63.5C
    BYTE  battLife;    // Portion of battery left, 0 to 255
    BYTE  battType;    // 1 = lithium, 2 = NiMH
    BYTE  pressure[NBR_WTTTS_PRESSURE_BYTES];
    BYTE  rpm;         // current RPM
    BYTE  lastReset;   // 1 = power-up, 2 = WDT, 3 = brownout reset
    BYTE  rfChannel;   // 11 through 26
    BYTE  currMode;    // 0 - entering deep sleep, 1 = normal, 2 = low-power
    WORD  rfcRate;
    WORD  rfcTimeout;
    WORD  streamRate;
    WORD  streamTimeout;
    WORD  pairingTimeout;
} WTTTS_RFC_PKT;

// Payload for set rate command.
typedef enum {
    SRT_RFC_RATE,                   // Sets RFC tx rate, in msecs
    SRT_RFC_TIMEOUT,                // Sets how long the WTTTS receiver remains active after sending a RFC
    SRT_STREAM_RATE,                // Sets the rate at which stream packets are reported
    SRT_STREAM_TIMEOUT,             // Sets how long the WTTTS will send stream packets before automatically stopping
    SRT_PAIR_TIMEOUT,               // Sets how long before the WTTTS enters 'deep sleep' if it can't connect
	SRT_SOLENOID_TIMEOUT,			// Used by PIB board to determine the max time a solenoid can be in the active state
    NBR_SET_RATE_TYPES
} WTTTS_RATE_TYPE;

typedef struct {
    BYTE  rateType;                 // One of the SRT_ enums from above
    BYTE  rfu;                      // Must always be zero
    WORD  newValue;                 // Value in secs or msecs, see enum defs above
} WTTTS_RATE_PKT;

// The following payloads are used to get / set configuration information.
// Configuration data is stored in in pages. Each page consists of 16 bytes
// of data. All pages can stored in the WTTTS can be read. However, the host
// can only write to a subset of all pages.
#define NBR_WTTTS_CFG_PAGES        32
#define WTTTS_CFG_FIRST_HOST_PAGE  16
#define NBR_BYTES_PER_WTTTS_PAGE   16

#define WTTTS_PG_RESULT_SUCCESS    0
#define WTTTS_PG_RESULT_BAD_PG     1

#define MPL_HDR_FROM_HOST	0x81	// packet sent by Host
#define MPL_HDR_FROM_DEVICE	0x15	// packet sent by Device

typedef struct {
    BYTE pageNbr;
} WTTTS_CFG_ITEM;

typedef struct {
    BYTE pageNbr;
    BYTE result;    // Always zero host to WTTTS; from WTTTS: 0 = success, 1 = invalid page
    BYTE pageData[NBR_BYTES_PER_WTTTS_PAGE];
} WTTTS_CFG_DATA;

// Unit version information.
#define WTTTS_FW_VER_LEN   4

typedef struct {
    BYTE hardwareSettings;          // Physical jumper settings on sub
    BYTE fwVer[WTTTS_FW_VER_LEN];   // Firmware version info
} WTTTS_VER_PKT;

// Set RF Channel command
typedef struct {
    BYTE chanNbr;     // 11 through 26
} WTTTS_SET_CHAN_PKT;

// All packet payloads are defined in the following union
typedef union {
    WTTTS_STREAM_DATA  streamData;
    WTTTS_RFC_PKT      rfcPkt;
    WTTTS_CFG_DATA     cfgData;
    WTTTS_CFG_ITEM     cfgRequest;
    WTTTS_VER_PKT      verPkt;
    WTTTS_RATE_PKT     ratePkt;
    WTTTS_SET_CHAN_PKT chanPkt;
} WTTTS_DATA_UNION;

//#pragma pack(pop)


#define SIZEOF_TEC_CHECKSUM  1

#define MIN_TEC_PKT_LEN      ( sizeof( WTTTS_PKT_HDR) + SIZEOF_TEC_CHECKSUM )
#define MAX_WTTTS_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )


// Packet header defines
#define WTTTS_HDR_RX   0x29       // inbound from WTTTS
#define WTTTS_HDR_TX   0x46       // outbound to WTTTS

// Packet type defintions: outbound to WTTTS.
//#define WTTTS_CMD_NO_CMD           0x80
//#define WTTTS_CMD_START_STREAM     0x82
//#define WTTTS_CMD_STOP_STREAM      0x84
//#define WTTTS_CMD_SET_RATE         0x86
//#define WTTTS_CMD_QUERY_VER        0x8A
//#define WTTTS_CMD_ENTER_DEEP_SLEEP 0x8C
//#define WTTTS_CMD_SET_RF_CHANNEL   0x8E
//#define WTTTS_CMD_SET_CFG_DATA     0xA0
//#define WTTTS_CMD_GET_CFG_DATA     0xA2
//
//// Packet type definitions: inbound from WTTTS
//#define WTTTS_RESP_STREAM_DATA     0x24
//#define WTTTS_RESP_REQ_FOR_CMD     0x26
//#define WTTTS_RESP_VER_PKT         0x28
//#define WTTTS_RESP_CFG_DATA        0x2A
//
//bool HaveWTTTSCmdPkt ( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, WTTTS_DATA_UNION& pktData );
//bool HaveWTTTSRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, WTTTS_DATA_UNION& pktData );
    // Scans the passed buffer for a WTTTS command or response. Returns true if
    // a packet is found, in which case pktHdr and pktData vars are populated.
    // Returns false if no packet is found. In either case, data in pBuff is
    // shifted out (if necessary) and the remaining count of bytes in the buffer
    // is returned in param buffLen.


// Packet header
typedef struct  __attribute__((__packed__)){
    BYTE pktHdr;       // TEC_HDR_... define
    BYTE devAddr;       // device address.
    BYTE pktType;      // TEC_CMD_... or TEC_RESP_... define
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
} TEC_PKT_HDR_T;

//#define TEC_PKT_HDR			0x73	// message comming from TEC board
//#define PIB_PKT_HDR			0x7A	// message comming from PIB board
//#define SENS_PKT_HDR		0x7D	// message comming from sensor board
//#define BROADCAST_ADDRESS	0xFF
//#define TEC_ADDRESS			0xFE
//#define PIB_ADDRESS			0x11
//#define CMD_SET_CONTACT		0x31
//#define RSP_SET_CONTACT		0x32
//#define CMD_GET_BAT_STATE	0x33
//#define RSP_GET_BAT_STATE	0x34
//#define CMD_GET_SOL_CURRENT	0x35
//#define RSP_GET_SOL_CURRENT	0x36
//#define CMD_GET_SER_NBR		0x37
//#define RSP_GET_SER_NBR		0x38
//#define CMD_GET_VERSION		0x39
//#define RSP_GET_VERSION		0x3A


void SendVersionInfo(void);

void ConvertLongTo3Bytes(signed long slValue, BYTE *pData);

void ConvertLongTo4Bytes(signed long slValue, BYTE *pData);

//BOOL GetCommandStatus(void);



#endif
