//******************************************************************************
//
//  TypeDefs.h: Type Definitions for code
//
//      Copyright (c) 2008, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  All the type definitions needed for the program
//
//******************************************************************************



#ifndef TYPEDEFS_H

    #define TYPEDEFS_H
    
    typedef unsigned char   BOOL;
    typedef unsigned int    WORD;
    typedef unsigned long   DWORD;
    typedef unsigned char   BYTE;
    typedef signed char   	SBYTE;

    typedef signed long		UVOLTS;
    
    #ifndef PCHAR
    	typedef char*       PCHAR;
	#endif

    #define FALSE           0
    #define TRUE            1

    #define NULL            0

    #ifndef LOBYTE
        #define LOBYTE(w)       ((BYTE)(w))
    #endif

    #ifndef HIBYTE
        #define HIBYTE(w)       ((BYTE)(((WORD)(w) >> 8) & 0xFF))
    #endif

    #ifdef LOWORD
        #define LOWORD(l)       ((WORD)(DWORD)(l))
    #endif

    #ifndef HIWORD
        #define HIWORD(l)       ((WORD)((((DWORD)(l)) >> 16) & 0xFFFF))
    #endif

    // When working with DWORDs and WORDs, the following union
    // facilitates conversion to / from bytes.

    typedef union {
        DWORD dwData;
        WORD  wData[2];
        BYTE  byData[4];
    } DWORD_BUFF;


    // Some handy macros for the date conversion work...

    #define IsNum( aChar )      ( ( ( aChar >= '0' ) && ( aChar <= '9' ) ) ? 1 : 0 )
    #define CharToInt( aChar )  ( ( ( aChar >= '0' ) && ( aChar <= '9' ) ) ? aChar - '0' : 0 )

    #define BIT_SIZEOF_BYTE 8
    #define BIT_SIZEOF_NIBBLE 4
    
    #define INT16_MAX_POSITIVE	32767 
    #define INT16_MAX_NEGATIVE  -32768
    
	#define INT24_MAX_POSITIVE	8388607
	#define INT24_MAX_NEGATIVE	-8388608

	#define LOW_BYTE	0
	#define HIGH_BYTE	1
	#define WORD_SIZE	2

#endif


