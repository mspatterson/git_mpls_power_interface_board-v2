/*
 * rs485.c
 *
 *  Created on: 2016-10-03
 *      Author: Owner
 */


#include "rs485.h"
#include <string.h>
#include <stdlib.h>

#define BUF_LENGTH 						64
#define MAX_CMD_LINE_LEN                BUF_LENGTH
#define MAX_RESP_LINE_LEN               BUF_LENGTH

// ms - the timer interrupt period is 1ms, that's why we set the timeout to 2 to be sure
// it's going to be between 1 and 2 ms
#define RX485_DATA_TIMEOUT			10	// ms
#define TX_DATA_IDLE_TIME			3	// ms


static TIMERHANDLE thRs485StateTimer; 	// Handle to timer to manage current state

// Serial port definitions
static UART_OBJECT rs485UARTObj;

static BYTE byRs485RxBuffer[MAX_RESP_LINE_LEN];
static WORD wRxIndex;

// Tx buffer must be wide enough to hold any commands to the modem. Command data
// must remain in the buffer until the lower-layer UART is done sending the data.
static BYTE byRs485TxBuffer[MAX_CMD_LINE_LEN];
static WORD wTxCount;

//static BOOL ChangeChannelPending =FALSE;
//static BOOL ChangeRadioPowerPending = FALSE;
//static BYTE	currentChannel = 0;
//static BYTE newChannel = 0;


void DisableRS485(void)
{
	SetOutputPin(RS485_RX_EN, TRUE);
	SetOutputPin(RS485_TX_EN, FALSE);
}


void EnRXRS485(void)
{
	SetOutputPin(RS485_RX_EN, FALSE);
	SetOutputPin(RS485_TX_EN, FALSE);
}

void EnTXRS485(void)
{
	SetOutputPin(RS485_RX_EN, TRUE);
	SetOutputPin(RS485_TX_EN, TRUE);
}

void EnTXRXRS485(void)
{
	SetOutputPin(RS485_RX_EN, FALSE);
	SetOutputPin(RS485_TX_EN, TRUE);
}

//******************************************************************************
//
//  Function: InitRadioInterface
//
//  Arguments: none 
//
//  Returns: TRUE on success, FALSE if the serial port could not be opened
//
//  Description: Must be the first function called in this routine. This
//               function initializes all internal static variables.
//
//******************************************************************************
BOOL InitRs485Interface( void)
{

    // Init state vars
	if(thRs485StateTimer==0)
	{
		thRs485StateTimer = RegisterTimer();
	}

    
    // Clear the comm buffers
    wRxIndex = 0;
    wTxCount = 0;

	UARTA0Init();
	SetUSCIMode(USCI_UART_MODE);
	UARTA0RxEnable();	// enable RX
	StartTimer( thRs485StateTimer );

//	EnTXRS485();
	EnRXRS485();

	// check if we can communicate with all boards
	// TODO

    // Init is good!
    return TRUE;
}


//******************************************************************************
//
//  Function: atRs485TxISR
//
//  Arguments: none
//
//  Returns: none
//
//  Description:	This function is called in the context of an UART TX ISR.
//
//
//******************************************************************************
void atRs485TxISR( void )
{

    if( rs485UARTObj.wTxQIndex < rs485UARTObj.wTxQSize )
    {
        UCA0TXBUF = rs485UARTObj.ptrTxQ[rs485UARTObj.wTxQIndex];
        rs485UARTObj.wTxQIndex++;
        
    }
    else
    {
    	__delay_cycles(4000);
        // Disable TX interrupt
        UARTA0TxDisable();
        UARTA0RxEnable();
        rs485UARTObj.bTxQSending = FALSE;

        EnRXRS485();		// enabe rs485 receiver
    }
}


//******************************************************************************
//
//  Function: atRs485RxISR
//
//  Arguments: none
//
//  Returns: none
//
//  Description:	This function is called in the context of an UART RX ISR.
//
//
//******************************************************************************
void atRs485RxISR( void )
{
 
	if( wRxIndex < MAX_RESP_LINE_LEN )
    {
    	byRs485RxBuffer[wRxIndex] = UCA0RXBUF;
        ++wRxIndex;
		ResetTimer( thRs485StateTimer, RX485_DATA_TIMEOUT );
    }
      
}


//******************************************************************************
//
//  Function: ReceiverHasData
//
//  Arguments: void.
//
//  Returns: TRUE/FALSE
//
//  Description: Call this function to check if packet has been received
//				the end of packet for now is received bytes >0, and nothing has been received for 1ms
//******************************************************************************
BOOL rs485ReceiverHasData(void)
{
	BOOL bResult = FALSE;
	
	if (wRxIndex>0)
		if(TimerExpired(thRs485StateTimer))
			bResult = TRUE;
			
	return bResult;		
}

//******************************************************************************
//
//  Function: ReadReceiverData
//
//  Arguments: void.
//
//  Returns: Bytes actually returned
//
//  Description: Copy the data to the supplied buffer
//
//******************************************************************************
WORD rs485ReadReceiverData(BYTE *pData, WORD maxSize)
{
	WORD wCnt;
	
	wCnt = 0;
	UARTA0RxDisable();		// disable RX interrupt

	while((wRxIndex)&&(wCnt<maxSize))
	{
		*pData = byRs485RxBuffer[wCnt];
		pData++;
		wCnt++;
		wRxIndex--;
	}
	wRxIndex = 0;			// flush the byRs485RxBuffer
	UARTA0RxEnable();		// enable RX interrupt
	return wCnt;
}


//******************************************************************************
//
//  Function: rs485SendBinaryData
//
//  Arguments: pointer to the data, number of data
//
//  Returns: TRUE/FALSE.
//
//  Description: start the UART transmission.
//
//******************************************************************************
BOOL rs485SendBinaryData( BYTE* pData, WORD wDataLen )
{
//	WORD wBuffLen =0;

	EnTXRS485();
	__delay_cycles(4000);
	memcpy( byRs485TxBuffer, pData, wDataLen );
    wTxCount = wDataLen;
//  	FlushUartSerialTxQueue( &rs485UARTObj );

	if( !UARTA0SendBufferInt(&rs485UARTObj, byRs485TxBuffer, wTxCount ) )
    	return FALSE;
    	
    return TRUE;	
}

//******************************************************************************
//
//  Function: rs485SendCommand
//
//  Arguments: pointer to the data, number of data
//
//  Returns: TRUE/FALSE.
//
//  Description: start the UART transmission.
//
//******************************************************************************
BOOL rs485SendCommand( BYTE* pData, WORD wDataLen  )
{

	memcpy( byRs485TxBuffer, pData, wDataLen );

	EnTXRS485();
	__delay_cycles(4000);
    wTxCount = wDataLen;

//  	FlushUartSerialTxQueue( &rs485UARTObj );

	if( !UARTA0SendBufferInt(&rs485UARTObj, byRs485TxBuffer, wTxCount ) )
    	return FALSE;

    return TRUE;
}


//******************************************************************************
//
//  Function: IsRs485Sending
//
//  Arguments: none
//
//  Returns: TRUE/FALSE.
//
//  Description: return the TX sending status
//
//******************************************************************************
BOOL IsRs485Sending(void)
{
	return rs485UARTObj.bTxQSending;
}



