#ifndef I2CSWITCH_H_
#define I2CSWITCH_H_

#include "..\drivers.h"

#define I2CSWITCH_ADDRESS	0x73
#define I2CSW_CHANNEL0		0x01
#define I2CSW_CHANNEL1		0x02

void I2CSwitchInit(void);
void I2CSwitchSelChannel(BYTE channel);
BOOL I2CSwitchTxDone(void);
void I2CSwitchReset(void);
void I2CSwitchGetData(BYTE *pData);
BOOL I2CSwitchDataReady(void);
void I2CSwitchReadDataStart(void);

#endif /*I2CSWITCH_H_*/
