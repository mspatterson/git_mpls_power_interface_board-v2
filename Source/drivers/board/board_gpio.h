#ifndef BOARD_GPIO_H_
#define BOARD_GPIO_H_


#include "..\drivers.h"


BYTE GetCardAddress(void);



//void DisableRS485(void);
//void EnTXRS485(void);
//void EnRXRS485(void);
void SetSensorAddInterrupt(void);
void SetSensorEnInterrupt(void);

void SetContact(WORD nbrContact);
void ResetContact(WORD nbrContact);
void SetContactResetOthers(WORD nbrContact);
void SetContactState(WORD nbrContact, BOOL bState);
void SetSolenoid(WORD nbrContact);
void ResetSolenoid(WORD nbrContact);
void SetSolenoidStateResetOthers(WORD nbrContact);
void SetSolenoidState(WORD nbrContact, BOOL bState);
void EnableSolenoidPower(void);
void DisableSolenoidPower(void);
void SetContactByBitMask(BYTE contBitMask);
void SetSolenoidByBitMask(BYTE contBitMask);
void ResetSolenoidState(void);
void EnableExtPower(void);
void DisableExtPower(void);

#endif /*BOARD_GPIO_H_*/
