/*
 * board_gpio.c
 *
 *  Created on: 2012-12-03
 *      Author: DD
 */
#include "board_gpio.h"

#define DEBOUNCE_TIME	500


const BYTE CONTACT_OUTPUTS[4]=
{
		CC_OUT_1,
		CC_OUT_2,
		CC_OUT_3,
		CC_OUT_4
};

const BYTE SOLENOID_OUTPUTS[6]=
{
		SOLEN_EN_1,
		SOLEN_EN_2,
		SOLEN_EN_3,
		SOLEN_EN_4,
		SOLEN_EN_5,
		SOL_TEST_EN
};

static BYTE cardAddress;
static BYTE cardAddCounter;
enum MCU_STATES{IDLE, ACTIVE};
enum MCU_STATES mcuState;




//// Read the 5 inputs to determine the card address
//BYTE GetCardAddress(void)
//{
//	BYTE byVar;
//	BYTE i;
//
//	byVar = 0;
//	for(i=0;i<sizeof(CardAddressInputs); i++)
//	{
//		SetPinAsInput(CardAddressInputs[i]);
//		EnableInputPullUp(CardAddressInputs[i]);
//	}
//
//	for(i=0;i<sizeof(CardAddressInputs); i++)
//	{
//		if(ReadInputPin(CardAddressInputs[i]))
//				byVar |= 1<<i;
//	}
//
////	byVar++;  	// increase with one so the address starts from 1
//	cardAddress = 0x1F&(~byVar);	// invert so a resistor presence means 'one'
//	cardAddress++;// increase with one so the address starts from 1
//	return cardAddress;
//
//}




//******************************************************************************
//
//  Function: SetSensorEnInterrupt
//
//  Arguments: none
//
//  Returns: none
//
//  Description: set interrupt on falling edge of SENSOR-ENA P2.0
//
//******************************************************************************
void SetSensorEnInterrupt(void)
{
	__bic_SR_register(GIE);
	P2IE = 0x01;                             // P2.0 interrupt enabled
	P2IES |= 0x01;                           // set P2.0 High/Low edge
	P2IFG = 0x00;                           // P2 IFG cleared
	mcuState = IDLE;
	__bis_SR_register(GIE);
}

//******************************************************************************
//
//  Function: SetSensorAddInterrupt
//
//  Arguments: none
//
//  Returns: none
//
//  Description: set interrupt on rising edge of SENSOR-ADD P2.1
//
//******************************************************************************
void SetSensorAddInterrupt(void)
{
	__bic_SR_register(GIE);
	P2IE = 0x02;                             // P2.1 interrupt enabled
	P2IES &= ~0x02;                           // set P2.0 Low/High edge
	P2IFG = 0x00;                           // P2 IFG cleared
	cardAddCounter = 0;						// zero the address counter
	mcuState = ACTIVE;
	__bis_SR_register(GIE);
}

/***************************************************************************
 * Port 2 interrupt service routine
 * In sleep mode the MCU will wait for SENSOR-ENA to become low.
 * After that it will start counting every low/high transition on SENSOR-ADD.
 * When the counter is equal to the board address the MCU exits sleep mode.
 ***************************************************************************/
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{

	switch(mcuState)
	{
	case IDLE:
		if(P2IFG&0x01)							// interrupt from P2.0-SENSOR-ENA
		{
			//SetSensorAddInterrupt();
			P2IE = 0x00;					// P2 disable interrupts
			LPM3_EXIT;						// exit sleep mode
		}
		P2IFG = 0;                      		// P2 IFG cleared
		break;
	case ACTIVE:
		if(P2IFG&0x02)							// interrupt from P2.1-SENSOR-ADD
		{
			cardAddCounter++;					// increment the address counter
			if(cardAddCounter == cardAddress)	// if it's your turn exit sleep mode
			{
				P2IE = 0x00;					// P2 disable interrupts
				LPM3_EXIT;						// exit sleep mode
			}
		}
		P2IFG = 0;                      		// P2 IFG cleared
		break;
	default:
		P2IFG = 0;                      		// P2 IFG cleared
		break;
	}
}

//******************************************************************************
//
//  Function: SetContact
//
//  Arguments: WORD nbrContact
//
//  Returns: none
//
//  Description: set the corresponding contact output
//
//******************************************************************************
void SetContact(WORD nbrContact)
{
	if(nbrContact<sizeof(CONTACT_OUTPUTS))
	{
		SetOutputPin(CONTACT_OUTPUTS[nbrContact], TRUE);
	}
}

//******************************************************************************
//
//  Function: ResetContact
//
//  Arguments: WORD nbrContact
//
//  Returns: none
//
//  Description: reset the corresponding contact output
//
//******************************************************************************
void ResetContact(WORD nbrContact)
{
	if(nbrContact<sizeof(CONTACT_OUTPUTS))
	{
		SetOutputPin(CONTACT_OUTPUTS[nbrContact], FALSE);
	}
}

//******************************************************************************
//
//  Function: SetContactResetOthers
//
//  Arguments: WORD nbrContact
//
//  Returns: none
//
//  Description: set the corresponding contact output and reset all others.
//
//******************************************************************************
void SetContactResetOthers(WORD nbrContact)
{
	WORD i;
	if(nbrContact<sizeof(CONTACT_OUTPUTS))
	{
		for(i=0;i<sizeof(CONTACT_OUTPUTS);i++)
		{
			SetOutputPin(CONTACT_OUTPUTS[i], FALSE);
		}

		SetOutputPin(CONTACT_OUTPUTS[nbrContact], TRUE);
	}

}

//******************************************************************************
//
//  Function: SetContactState
//
//  Arguments: WORD nbrContact
//				BOOL bState
//
//  Returns: none
//
//  Description: set the corresponding contact output in the provided state.
//
//******************************************************************************
void SetContactState(WORD nbrContact, BOOL bState)
{
	if(nbrContact<sizeof(CONTACT_OUTPUTS))
	{
		SetOutputPin(CONTACT_OUTPUTS[nbrContact], bState);
	}
}

//******************************************************************************
//
//  Function: SetSolenoid
//
//  Arguments: WORD nbrContact
//
//  Returns: none
//
//  Description: set the corresponding solenoid output.
//
//******************************************************************************
void SetSolenoid(WORD nbrContact)
{
	if(nbrContact<sizeof(SOLENOID_OUTPUTS))
	{
		SetOutputPin(SOLENOID_OUTPUTS[nbrContact], TRUE);
	}
}

//******************************************************************************
//
//  Function: ResetSolenoid
//
//  Arguments: WORD nbrContact
//
//  Returns: none
//
//  Description: reset the corresponding solenoid output.
//
//******************************************************************************
void ResetSolenoid(WORD nbrContact)
{
	if(nbrContact<sizeof(SOLENOID_OUTPUTS))
	{
		SetOutputPin(SOLENOID_OUTPUTS[nbrContact], FALSE);
	}
}

//******************************************************************************
//
//  Function: SetSolenoidStateResetOthers
//
//  Arguments: WORD nbrContact
//
//  Returns: none
//
//  Description: set the corresponding solenoid output and reset all others
//
//******************************************************************************
void SetSolenoidStateResetOthers(WORD nbrContact)
{
	WORD i;
	if(nbrContact<sizeof(SOLENOID_OUTPUTS))
	{
		for(i=0;i<sizeof(SOLENOID_OUTPUTS);i++)
		{
			SetOutputPin(SOLENOID_OUTPUTS[i], FALSE);
		}

		SetOutputPin(SOLENOID_OUTPUTS[nbrContact], TRUE);
		EnableSolenoidPower();
	}

}

//******************************************************************************
//
//  Function: SetSolenoidState
//
//  Arguments: WORD nbrContact
//				BOOL bState
//
//  Returns: none
//
//  Description: set the corresponding solenoid output in the provided state.
//
//******************************************************************************
void SetSolenoidState(WORD nbrContact, BOOL bState)
{
	if(nbrContact<sizeof(SOLENOID_OUTPUTS))
	{
		SetOutputPin(SOLENOID_OUTPUTS[nbrContact], bState);
	}
}


//******************************************************************************
//
//  Function: EnableSolenoidPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description: enable the power to the solenoid drivers.
//
//******************************************************************************
void EnableSolenoidPower(void)
{
	SetOutputPin(EN_12V, TRUE);
}

//******************************************************************************
//
//  Function: DisableSolenoidPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description: disable the power to the solenoid drivers.
//
//******************************************************************************
void DisableSolenoidPower(void)
{
	SetOutputPin(EN_12V, FALSE);
}


//******************************************************************************
//
//  Function: SetContactByBitMask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: set the contacts based on the input bit mask. '1' set the output high. '0' set the output low.
//
//******************************************************************************
void SetContactByBitMask(BYTE contBitMask)
{
	WORD i;

	for(i=0;i<sizeof(CONTACT_OUTPUTS);i++)
	{
		if(contBitMask&(1<<i))
			SetOutputPin(CONTACT_OUTPUTS[i], TRUE);
		else
			SetOutputPin(CONTACT_OUTPUTS[i], FALSE);
	}

}

//******************************************************************************
//
//  Function: SetSolenoidByBitMask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: set the solenoid outputs based on the input bit mask. '1' set the output high. '0' set the output low.
// 				Only one solenoid can be active at any given time
//******************************************************************************
void SetSolenoidByBitMask(BYTE contBitMask)
{
	WORD i;
	BOOL oneSolActive = FALSE;

	for(i=0;i<sizeof(SOLENOID_OUTPUTS);i++)
	{
		if(contBitMask&(1<<i))
		{
			if(oneSolActive==FALSE)
			{
				EnableSolenoidPower();
				SetOutputPin(SOLENOID_OUTPUTS[i], TRUE);
//				oneSolActive = TRUE;
			}
			else
				SetOutputPin(SOLENOID_OUTPUTS[i], FALSE);
		}
		else
			SetOutputPin(SOLENOID_OUTPUTS[i], FALSE);
	}

}


//******************************************************************************
//
//  Function: ResetSolenoidState
//
//  Arguments: none
//
//  Returns: none
//
//  Description: reset all solenoid outputs.
//
//******************************************************************************
void ResetSolenoidState(void)
{
	WORD i;

	for(i=0;i<sizeof(SOLENOID_OUTPUTS);i++)
	{
		SetOutputPin(SOLENOID_OUTPUTS[i], FALSE);
	}
	DisableSolenoidPower();
}


//******************************************************************************
//
//  Function: EnableExtPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description: enable the external power supply
//
//******************************************************************************
void EnableExtPower(void)
{
	SetOutputPin(RS485_PWR_EN, FALSE);
}

//******************************************************************************
//
//  Function: DisableExtPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description: disable the external power supply
//
//******************************************************************************
void DisableExtPower(void)
{
	SetOutputPin(RS485_PWR_EN, TRUE);
}



