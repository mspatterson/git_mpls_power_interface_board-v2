//******************************************************************************
//
//  WTTTSProtocolUtils.c: Communication protocol routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-03-26     DD          Initial Version
//
//******************************************************************************

#include "WTTTSProtocolUtils.h"

//#pragma package(smart_init)


//
// Private Declarations
//

// The WTTTS packet parser is table driven. To add a new command
// or response, do the following:
//
//   1. In the .h file, add a new WTTTS_CMD_ or WTTTS_RESP_ define
//      (if required)
//
//   2. Change the NBR_CMD_TYPES or NBR_RESP_TYPES define (if
//      a new cmd or response was defined).
//
//   3. If a new command or response has been defined, and that
//      item has a data payload, define a structure for the
//      payload in the .h file, and add a member of that structure
//      type to the WTTTS_DATA_UNION union definition.
//
//   4. Finally, add a row to (for new cmds or responses) or
//      modify an existing entry in teh m_pktInfo[] table.

#define NBR_CMD_TYPES  5   // Total number of TEC_CMD_... defines
#define NBR_RESP_TYPES 5   // Total number of WTTTS_RESP_... defines
#define RFC_RATE_DEFAULT	1000		//ms
#define RFC_TIMEOUT_DEFAULT	10			//ms
#define STREAM_RATE_DEFAULT	10			//ms
#define STREAM_TIMEOUT_DEFAULT	300		//seconds
#define PAIR_TIMEOUT_DEFAULT	900		//seconds
#define RF_PACKET_NUMBER		128

#define RFC_RATE_MIN		10		//ms
#define RFC_TIMEOUT_MIN		3			//ms
#define STREAM_RATE_MIN		9			//ms
#define STREAM_TIMEOUT_MIN	5		//seconds
#define PAIR_TIMEOUT_MIN	10		//seconds
//
//typedef struct {
//    BYTE pktHdr;        // Expected header byte
//    BYTE cmdRespByte;   // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
//    BYTE expDataLen;    // Required value for header pktLen param
//} WTTTS_PKT_INFO;
//
//static const WTTTS_PKT_INFO m_pktInfo[ NBR_CMD_TYPES + NBR_RESP_TYPES ] = {
//    { WTTTS_HDR_TX, WTTTS_CMD_NO_CMD,           0 },
//    { WTTTS_HDR_TX, WTTTS_CMD_START_STREAM,     0 },
//    { WTTTS_HDR_TX, WTTTS_CMD_STOP_STREAM,      0 },
//    { WTTTS_HDR_TX, WTTTS_CMD_SET_RATE,         sizeof( WTTTS_RATE_PKT ) },
//    { WTTTS_HDR_TX, WTTTS_CMD_QUERY_VER,        0 },
//    { WTTTS_HDR_TX, WTTTS_CMD_ENTER_DEEP_SLEEP, 0 },
//    { WTTTS_HDR_TX, WTTTS_CMD_SET_RF_CHANNEL,   sizeof( WTTTS_SET_CHAN_PKT ) },
//    { WTTTS_HDR_TX, WTTTS_CMD_GET_CFG_DATA,     sizeof( WTTTS_CFG_ITEM )     },
//    { WTTTS_HDR_TX, WTTTS_CMD_SET_CFG_DATA,     sizeof( WTTTS_CFG_DATA )     },
//    { WTTTS_HDR_RX, WTTTS_RESP_STREAM_DATA,     sizeof( WTTTS_STREAM_DATA )  },
//    { WTTTS_HDR_RX, WTTTS_RESP_REQ_FOR_CMD,     sizeof( WTTTS_RFC_PKT )      },
//    { WTTTS_HDR_RX, WTTTS_RESP_CFG_DATA,        sizeof( WTTTS_CFG_DATA )     },
//    { WTTTS_HDR_RX, WTTTS_RESP_VER_PKT,         sizeof( WTTTS_VER_PKT )      },
//};


//typedef struct {
//    BYTE pktHdr;        // Expected header byte
//    BYTE cmdRespByte;   // One of the TEC_CMD_... or TEC_RESP_ defines
//    BYTE expDataLen;    // Required value for header pktLen param
//} TEC_PKT_INFO;
//
//static const TEC_PKT_INFO m_pktInfo[ NBR_CMD_TYPES ] = {
////    { MPL_HDR_FROM_HOST, CMD_SET_CONTACT,		2 },
////    { MPL_HDR_FROM_HOST, CMD_GET_BAT_STATE,   0 },
////    { MPL_HDR_FROM_HOST, CMD_GET_SOL_CURRENT, 0 },
////    { MPL_HDR_FROM_HOST, CMD_GET_SER_NBR,     0 },
////    { MPL_HDR_FROM_HOST, CMD_GET_VERSION,     0 },
//};


//static WORD wtttsTimeParams[NBR_SET_RATE_TYPES];
//
//const WORD wtttsTimeParamsDefault[NBR_SET_RATE_TYPES]= {
//		RFC_RATE_DEFAULT,
//		RFC_TIMEOUT_DEFAULT,
//		STREAM_RATE_DEFAULT,
//		STREAM_TIMEOUT_DEFAULT,
//		PAIR_TIMEOUT_DEFAULT,
//};


//const WORD wtttsTimeParamsMin[NBR_SET_RATE_TYPES]= {
//		RFC_RATE_MIN,
//		RFC_TIMEOUT_MIN,
//		STREAM_RATE_MIN,
//		STREAM_TIMEOUT_MIN,
//		PAIR_TIMEOUT_MIN,
//};
//static WTTTS_RFC_PKT	wtttsRfcPacket;

//static BYTE radioPacket[RF_PACKET_NUMBER];
////static BYTE wtttsPacket[RF_PACKET_NUMBER];
//static BYTE *wtttsPacket;
//static BYTE sequenceNumber;
//static BOOL commNoCommand;
//static BOOL commStartStream;
//static BOOL commStopStream;
//static BOOL commPowerDown;
//static BYTE currMode =1;
//static BYTE pageNumber;
//static BYTE flashWriteResult;
//static BOOL  havePkt = FALSE;

TEC_PKT_HDR_T* pktHdr;
BYTE* pktData;
WTTTS_CFG_DATA* cfgData;
WTTTS_CFG_ITEM* cfgItem;


void word2ByteArray(void *pVoid, WORD wValue);





void ConvertLongTo3Bytes(signed long slValue, BYTE *pData)
{
	if(slValue>INT24_MAX_POSITIVE)
		slValue = INT24_MAX_POSITIVE;

	if(slValue < INT24_MAX_NEGATIVE)
			slValue = INT24_MAX_NEGATIVE;

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);

}


void ConvertLongTo4Bytes(signed long slValue, BYTE *pData)
{

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);
	pData[3] = 	(BYTE) (slValue>>24);

}

void word2ByteArray(void *pVoid, WORD wValue)
{
	BYTE *pByte;
	WORD wTemp;
	wTemp = wValue;	// function to do
	pByte = (BYTE*)	pVoid;
	*pByte++ = (BYTE)wTemp;
	*pByte = (BYTE)(wTemp>>8);
}

