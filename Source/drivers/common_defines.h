#ifndef COMMON_DEF_H_
#define COMMON_DEF_H_

#include "drivers.h"


#define TX_ACK		0XB3
#define TX_NACK		0XDC
#define COMMAND		0X53
#define TX_DATA		0X29
#define RADIO_POWER_DOWN	0XA7
#define RADIO_CHANGE_CHANNEL	0XA9
#define RFC_RESPONSE_TIME	5	//ms
#define BAT_INPUTS			3

#define CHANNEL_CYCLE_MS		500
#define UPDATE_CHANNEL_NBR_TR	6
#define NBR_RESP_CONNECTION		3
#define HUNTING_MODE_TIMEOUT_SEC	900	// SEC = 15MIN
#define IDLE_MODE_TIMEOUT_SEC		30	// sec


#define TEST_CONTACT_TIME_SEC	3 // seconds
#define TOTAL_CONTACTS	4
#define TOTAL_SOLENOIDS	5

#define USCI_UART_MODE	0
#define USCI_I2C_MODE	1

#define NBR_PTS_BOARDS	4

#define MASTER_PIB	0
#define SLAVE_PIB	1


// DEBUG VARS


#endif /*COMMON_DEF_H_*/
