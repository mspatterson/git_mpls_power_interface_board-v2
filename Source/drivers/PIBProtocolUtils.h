//******************************************************************************
//
//  PIBProtocolUtils.h:
//
//      Copyright (c) 2016, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//	2016-Oct-04		DD 			Initial Implementation
//*******************************************************************************


#ifndef PIBProtocolUtilsH
#define PIBProtocolUtilsH

#include "drivers.h"


// Packet formats (inbound and outbound). All structures are byte-aligned
//#pragma pack(push, 1)

// Packet header
typedef struct {
    BYTE pktHdr;       // PIB_HDR_... define
    BYTE pktType;      // defines what the packet is for. Command or Response...
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
} PIB_PKT_HDR;


typedef struct {
	BYTE solenoidCtrl;	// bit flags to activate or deactivate the corresponding solenoid
	BYTE contactCtrl;	// bit flags to close or open the contacts.
	BYTE solenoidTimeout;	// 0 to 255 seconds. The maximum time a solenoid can be in active state, measured in seconds. Default is 10 sec.
} PIB_SET_PARAM_CMD_PKT;


typedef struct {
	BYTE batVoltage[WORD_SIZE];		// Battery voltage in mV
	BYTE solenoidStatus;	// Solenoid number 0 to 5. 0 = all solenoids off
	BYTE solenoidTimeout;	// The maximum time a solenoid can be in active state, measured in seconds. Default is 10 sec. Range is 0 to 255 seconds.
	BYTE solenoidCurrent;	// Solenoid current in mA
	BYTE contactStatus;		// Lower four bits represents outputs 1, 2, 3 or 4. 0=off, 1=on.
} PIB_RSP_PKT;

typedef struct {
    BYTE hardVer[4];
	BYTE frmVers[4];        //
    BYTE devSerNum[32];      //
} PIB_VERSIONS_RSP_PKT;


typedef enum{
	MODE_HUNTING,
	MODE_ACTIVE,
	MODE_SLEEP,
	MODE_NBRS
}MPL_OP_MODES;


#define SIZEOF_PIB_CHECKSUM  1

#define MIN_PIB_PKT_LEN      ( sizeof( PIB_PKT_HDR ) + SIZEOF_PIB_CHECKSUM )
#define MAX_PIB_PKT_LEN      ( sizeof( PIB_PKT_HDR ) + sizeof( PIB_RSP_PKT ) + SIZEOF_PIB_CHECKSUM )


//#define PIB_ADDRESS		17

// Packet Header sent by the Master TEC
#define TEC_S_CMD_HDR	0x20	// Slave TEC Command Header. Used in packets sent by the Master TEC board.
#define PIB_M_CMD_HDR	0x10	// Master PIB Command Header. Used in packets sent by the Master TEC board.
#define PIB_S_CMD_HDR	0x30	// Slave PIB Command Header. Used in packets sent by the Master TEC board.
#define PTS_CMD_HDR		0x40	// PTS Command Header. Used in packets sent by the Master TEC board.


// Packet Header response sent by the Slaves.
#define TEC_S_RESP_HDR	0x25	// Slave TEC Response Header. Used in packets sent by the Slave TEC board.
#define PIB_M_RESP_HDR	0x15	// Master PIB Response Header. Used in packets sent by the Master PIB board.
#define PIB_S_RESP_HDR	0x35	// Slave PIB Response Header. Used in packets sent by the Master PIB board.
#define PTS_RESP_HDR	0x45	// PTS Response Header. Used in packets sent by the PTS board.

// Packet type definitions - commands:
#define PIB_GET_DATA		0x50
#define PIB_SET_PARAMS		0x52
#define TEC_S_GET_DATA		0x54
#define TEC_S_SET_PARAMS	0x56
#define PTS_GET_DATA		0x58
#define PTS_SET_PARAMS		0x5A
#define COM_GET_VERSIONS	0x5C

// Packet type definitions - responses:
//#define PIB_RSP_SET_PARAMS		0x72
//#define PIB_RSP_GET_PARAMS		0x74
//#define PTS_RSP_SET_PARAMS		0x76
//#define PTS_RSP_GET_PARAMS		0x78



void ReadDeviceRole(void);
void SetPibRole(BYTE pibMasterSlave);
void InitPIBInterface(void);

BOOL HavePIBPacket(BYTE *pBuff, WORD buffLen);
void ExecuteTecCommand(void);
void ExecSetParams(void);
void ExecGetParams(void);
void ExecGetVersion(void);
void UpdateSolenoidState(void);
BYTE GetLastCmd(void);


#endif	/*PIBProtocolUtilsH*/


