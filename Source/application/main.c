/*
 * main.c
 * 
 * 08 2016 - update for PCB-00962-01-1v0
 * 
 *
 */


#include "..\drivers\drivers.h"
#include <string.h>
#include <stdlib.h>


#define ALL_CHANNELS_OFFSET_DONE	0x7F	//0x3F
#define ZB_NETWORK_TIMEOUT		10
#define SAMPLES_IN_PACKET		2

#define ADC_OFFSET_SAMPLES		1000
#define CMD_START				0x23
#define CMD_STOP				0x34


static TIMERHANDLE thGenTimer;
static TIMERHANDLE thTimerSec1;


static BYTE bDataArray[20];
static BYTE pUartRxData[100];

signed long gyroSumAverage;
signed long slVar;


typedef struct{
	WORD ledActivatePeriod;
	WORD ledToggleTimes;
	WORD ledOnTime;
}LED_CONFIG_STRUCT;

LED_CONFIG_STRUCT	ledConfig;
#define LED_PERIOD	5000	// ms
#define LED_ON_TIME	300		// ms
#define ROLE_PIN_TIMEOUT		150 // ms

typedef enum{
	LED_INIT,
	LED_TOGGLE,
	LED_IDLE,
	LED_STATES
}BUTTON_LED_STATES;
BUTTON_LED_STATES	ledState;
///////////////////////////////////////////////////////////
//
// Local function declaration
//
///////////////////////////////////////////////////////////

void SampleData(BYTE bIndex);


void TestPowerDownMode(void);



void SendData(void);
void SendTestData(void);
void InitDataArray(void);
void InitTestSensorData(void);
void SendTestSensorData(void);


void CheckForAlarmCondition(void);
void CheckForAlarmConditionV1(void);

void SendAllData(void);


void PibCommTask(void);
void LedTask(void);
void TestContacts(void);
void LedPowerDown(void);


void SetPIBTestCommand(void);
void GetPIBTestCommand(void);

void SendTestDataV1(void);

void main(void)
{


	StopWatchdog();

	initMPS430Pins();
	EnableExtPower();
	BCSCTL1 = CALBC1_1MHZ;                    // Set DCO
	DCOCTL = CALDCO_1MHZ;
	SysTimerConfig();
	thGenTimer = RegisterTimer();
	StartTimer(thGenTimer);
	DisableRS485();
	__delay_cycles(1000);

//	UARTA0Init();
	ADC10_Init();
	InitPIBInterface();
	ledState = LED_INIT;
	ledConfig.ledActivatePeriod = LED_PERIOD;
	ledConfig.ledToggleTimes = 5;
	ledConfig.ledOnTime = 100;
//	EnableSolenoidPower();


	// at power up it takes about 100 ms for the signal that sets the MASTER/SLAVE to rise.
	// wait 150 ms before sample the pin
	ResetTimer(thGenTimer, ROLE_PIN_TIMEOUT);
	while(TimerExpired(thGenTimer)== FALSE);

	ReadDeviceRole();

	// test
	//	while(1)
//	{
//		getBatterVoltage_mV();
//	}
//	SendTestDataV1();	// test bit rate

#ifdef WATCHDOG_ENABLE
	SetWatchdog();
#endif



	while(1)
	{
		PibCommTask();

		UpdateSolenoidState();

		LedTask();

//		if(GetCurrentOpMode()==MODE_SLEEP)
//		{
//			LedPowerDown();
//			DisableSolenoidPower();
//			DisableRS485();
//			__bis_SR_register(LPM4_bits);     // Enter LPM4,
//			__no_operation();
//		}
//
#ifdef WATCHDOG_ENABLE
		ResetWatchdog();
#endif
	}

}


//******************************************************************************
//
//  Function: PibCommTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Manages communication and commands execution at high level.
//
//******************************************************************************
void PibCommTask(void)
{
	WORD pktLength;

	if(rs485ReceiverHasData())
	{
		pktLength = rs485ReadReceiverData(pUartRxData, sizeof(pUartRxData));

		if(HavePIBPacket(pUartRxData,pktLength))
		{
			ExecuteTecCommand();
		}

	}
}

//******************************************************************************
//
//  Function: LedTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: manages the LED states. Flashes 3 times at power up and then flashes once every 5 seconds.
//
//******************************************************************************
void LedTask( void )
{
    static BOOL ledToggle = TRUE;

    switch ( ledState )
    {
        case LED_INIT:
            if ( TimerExpired( thGenTimer ) )
            {
                if ( ledConfig.ledToggleTimes )
                {
                    SetOutputPin( SW_LED, ledToggle );
                    ledToggle ^= 1;
                    ledConfig.ledToggleTimes--;
                    ResetTimer( thGenTimer, ledConfig.ledOnTime );
                }
                else
                {
                    SetOutputPin( SW_LED, FALSE );
                    ledToggle = TRUE;
                    ledState = LED_IDLE;
                    ResetTimer( thGenTimer, ledConfig.ledActivatePeriod );
                }

            }
            break;

        case LED_TOGGLE:
            if ( TimerExpired( thGenTimer ) )
            {
                if ( ledConfig.ledToggleTimes )
                {
                    SetOutputPin( SW_LED, ledToggle );
                    ledToggle ^= 1;
                    ledConfig.ledToggleTimes--;
                    ResetTimer( thGenTimer, ledConfig.ledOnTime );
                }
                else
                {
                    SetOutputPin( SW_LED, FALSE );
                    ledToggle = TRUE;
                    ledState = LED_IDLE;
                    ResetTimer( thGenTimer, ledConfig.ledActivatePeriod );
                }

            }
            break;

        case LED_IDLE:
            if ( TimerExpired( thGenTimer ) )
            {

                switch ( GetLastCmd() )
                {
                    case 0:
                        ledConfig.ledToggleTimes = 1;	// flashes one times if it has not received any command
                        ledState = LED_TOGGLE;
                        break;

                    default:
                        ledConfig.ledToggleTimes = 3;	// flashes two times if it has received command
                        ledState = LED_TOGGLE;
                        break;
                }

            }
            break;
        default:
            ledState = LED_IDLE;
            ResetTimer( thGenTimer, ledConfig.ledActivatePeriod );
            break;
    }
}


void LedPowerDown(void)
{

	int cntOnCycle;
	int cntOffCycle;

	cntOnCycle = 30;
	cntOffCycle = 0;
	while(cntOnCycle>0)
	{
		SetOutputPin(SW_LED,TRUE);
		ResetTimer(thGenTimer,cntOnCycle);
		while(!TimerExpired(thGenTimer));
		SetOutputPin(SW_LED,FALSE);
		ResetTimer(thGenTimer,cntOffCycle);
		while(!TimerExpired(thGenTimer));
		cntOnCycle--;
		cntOffCycle++;

	}

}


/************************************
 ******* TEST FUNCTIONS**************
************************************/

void SendTestData(void)
{


	UARTA0Init();
	InitDataArray();
	UARTA0SendBufferPol((const BYTE*) bDataArray, 12 );
}


void TestContacts(void)
{
	WORD contactStateCounter = 0;
	WORD solStateCounter = 0;
	ResetSecTimer( thTimerSec1, (WORD) 5 );
	InitDataArray();
	UARTA0Init();
	EnableSolenoidPower();
	while(1)
	{
		if(SecTimerExpired(thTimerSec1))
		{
			SetContactResetOthers(contactStateCounter);
			SetSolenoidStateResetOthers(solStateCounter);

			contactStateCounter++;
			if(contactStateCounter>=TOTAL_CONTACTS)
				contactStateCounter = 0;

			solStateCounter++;
			if(solStateCounter>=TOTAL_SOLENOIDS)
				solStateCounter = 0;


			UARTA0SendBufferPol((const BYTE*) bDataArray, 10 );
			ResetSecTimer( thTimerSec1, (WORD) TEST_CONTACT_TIME_SEC );
		}
	}

}


void InitDataArray(void)
{
	unsigned int i;
//	BYTE bVar = 'a';
	BYTE bVar = 0xAA;
	for(i=0;i<sizeof(bDataArray);i++)
	{
		bDataArray[i] = bVar;
		bVar++;
	}

//	bDataArray[8] = '\n';
//	bDataArray[9] = '\r';
}



void SetPIBTestCommand(void)
{
	pUartRxData[0] = 0x51;
	pUartRxData[1] = 0x81;
	pUartRxData[2] = 0x11;
	pUartRxData[3] = 0x01;
	pUartRxData[4] = 0x01;
	pUartRxData[5] = 0xA6;

}


void GetPIBTestCommand(void)
{
	pUartRxData[0] = 0x51;
	pUartRxData[1] = 0x73;
	pUartRxData[2] = 0x11;
	pUartRxData[3] = 0x00;
	pUartRxData[4] = 0x38;

	if(HavePIBPacket(pUartRxData,5))
	{
		ExecuteTecCommand();
	}

}

void SendTestDataV1(void)
{


	UARTA0Init();
	InitDataArray();
	while(1)
	{
		UARTA0SendBufferPol((const BYTE*) bDataArray, 12 );
		__delay_cycles(20000);
	}
}

